package br.com.jorgerabellodev.ceepandroid.ui.recycler.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import br.com.jorgerabellodev.ceepandroid.R;
import br.com.jorgerabellodev.ceepandroid.model.Nota;
import br.com.jorgerabellodev.ceepandroid.ui.recycler.adapter.ListaNotasAdapter.NotaViewHolder;
import br.com.jorgerabellodev.ceepandroid.ui.recycler.adapter.listener.OnItemClickListener;
import java.util.Collections;
import java.util.List;

public class ListaNotasAdapter extends Adapter<NotaViewHolder> {


  private final List<Nota> notas;
  private final Context context;
  private OnItemClickListener onItemClickListener;

  @NonNull
  @Override
  public NotaViewHolder onCreateViewHolder(@NonNull ViewGroup view, int viewType) {

    View viewCriada = LayoutInflater
        .from(context)
        .inflate(R.layout.item_nota, view, false);

    return new NotaViewHolder(viewCriada);
  }

  @Override
  public void onBindViewHolder(@NonNull NotaViewHolder holder, int position) {

    Nota nota = notas.get(position);
    holder.vincula(nota);

  }

  @Override
  public int getItemCount() {
    return notas.size();
  }

  public ListaNotasAdapter(Context context, List<Nota> notas) {
    this.notas = notas;
    this.context = context;
  }

  public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
    this.onItemClickListener = onItemClickListener;
  }

  public void adiciona(Nota nota) {
    notas.add(nota);
    notifyDataSetChanged();
  }

  public void altera(int posicao, Nota nota) {
    notas.set(posicao, nota);
    notifyDataSetChanged();
  }

  public void remove(int posicao) {
    notas.remove(posicao);
    notifyItemRemoved(posicao);
  }

  public void troca(int posicaoInicio, int posicaoFim) {
    Collections.swap(notas, posicaoInicio, posicaoFim);
    notifyItemMoved(posicaoInicio, posicaoFim);
  }

  class NotaViewHolder extends ViewHolder {

    private final TextView titulo;
    private final TextView descricao;
    private Nota nota;

    NotaViewHolder(@NonNull View itemView) {
      super(itemView);
      titulo = itemView.findViewById(R.id.item_nota_titulo);
      descricao = itemView.findViewById(R.id.item_nota_descricao);
      itemView.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View view) {
          onItemClickListener.onItemClick(nota, getAdapterPosition());
        }
      });
    }

    void vincula(Nota nota) {
      this.nota = nota;
      preencheCampos(nota);
    }

    private void preencheCampos(Nota nota) {
      titulo.setText(nota.getTitulo());
      descricao.setText(nota.getDescricao());
    }

  }

}
