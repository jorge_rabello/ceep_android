package br.com.jorgerabellodev.ceepandroid.ui.activity;

import static br.com.jorgerabellodev.ceepandroid.ui.activity.NotaActivityConstantes.CHAVE_NOTA;
import static br.com.jorgerabellodev.ceepandroid.ui.activity.NotaActivityConstantes.CHAVE_POSICAO;
import static br.com.jorgerabellodev.ceepandroid.ui.activity.NotaActivityConstantes.POSICAO_INVALIDA;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import br.com.jorgerabellodev.ceepandroid.R;
import br.com.jorgerabellodev.ceepandroid.model.Nota;

public class FormulatioNotaActivity extends AppCompatActivity {

  public static final String TITULO_APP_BAR_ALTERAR_NOTA = "Alterar nota";
  public static final String TITULO_APP_BAR_CADASTRA_NOTA = "Cadastrar nota";
  private int posicaoRecebida = POSICAO_INVALIDA;

  private TextView descricao;
  private TextView titulo;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_formulario_nota);
    setTitle(TITULO_APP_BAR_CADASTRA_NOTA);
    inicializaCampos();

    Intent dadosRecebidos = getIntent();

    if (dadosRecebidos.hasExtra(CHAVE_NOTA)) {
      setTitle(TITULO_APP_BAR_ALTERAR_NOTA);
      posicaoRecebida = dadosRecebidos.getIntExtra(CHAVE_POSICAO, POSICAO_INVALIDA);

      Nota notaRecebida = (Nota) dadosRecebidos.getSerializableExtra(CHAVE_NOTA);

      if (notaRecebida != null) {
        preencheCampos(notaRecebida);
      }
    }

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_formulario_nota_salva, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    if (ehMenuSalvaNota(item)) {
      Nota notaCriada = criaNota();
      retornaNota(notaCriada);
      finish();
    }
    return super.onOptionsItemSelected(item);
  }

  private Nota criaNota() {
    return new Nota(titulo.getText().toString(), descricao.getText().toString());
  }

  private void preencheCampos(Nota notaRecebida) {
    titulo.setText(notaRecebida.getTitulo());
    descricao.setText(notaRecebida.getDescricao());
  }

  private void inicializaCampos() {
    titulo = findViewById(R.id.formulario_nota_titulo);
    descricao = findViewById(R.id.formulario_nota_descricao);
  }

  private void retornaNota(Nota nota) {
    Intent resultadoInsercao = new Intent();

    resultadoInsercao.putExtra(CHAVE_NOTA, nota);
    resultadoInsercao.putExtra(CHAVE_POSICAO, posicaoRecebida);

    setResult(RESULT_OK, resultadoInsercao);
  }

  private boolean ehMenuSalvaNota(@NonNull MenuItem item) {
    return item.getItemId() == R.id.menu_formulario_salva_nota;
  }

}
