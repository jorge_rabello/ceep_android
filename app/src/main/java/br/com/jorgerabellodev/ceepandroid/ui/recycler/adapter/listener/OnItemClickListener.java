package br.com.jorgerabellodev.ceepandroid.ui.recycler.adapter.listener;

import br.com.jorgerabellodev.ceepandroid.model.Nota;

public interface OnItemClickListener {

  void onItemClick(Nota nota, int posicao);
}
