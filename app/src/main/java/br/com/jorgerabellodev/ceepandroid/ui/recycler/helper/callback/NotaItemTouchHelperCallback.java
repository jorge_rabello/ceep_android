package br.com.jorgerabellodev.ceepandroid.ui.recycler.helper.callback;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.ItemTouchHelper.Callback;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import br.com.jorgerabellodev.ceepandroid.dao.NotaDAO;
import br.com.jorgerabellodev.ceepandroid.ui.recycler.adapter.ListaNotasAdapter;

public class NotaItemTouchHelperCallback extends Callback {

  private final ListaNotasAdapter adapter;

  public NotaItemTouchHelperCallback(ListaNotasAdapter adapter) {
    this.adapter = adapter;
  }

  @Override
  public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull ViewHolder viewHolder) {
    int marcacoesDeDeslize = ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT;
    int marcacoesParaArrastar =
        ItemTouchHelper.DOWN | ItemTouchHelper.UP | ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT;
    return makeMovementFlags(marcacoesParaArrastar, marcacoesDeDeslize);
  }

  @Override
  public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull ViewHolder viewHolder,
      @NonNull ViewHolder target) {
    int posicaoInicio = viewHolder.getAdapterPosition();
    int posicaoFim = target.getAdapterPosition();
    trocaNotas(posicaoInicio, posicaoFim);
    return true;
  }

  @Override
  public void onSwiped(@NonNull ViewHolder viewHolder, int direction) {
    int posicaoDaNotaDeslizada = viewHolder.getAdapterPosition();
    removeNota(posicaoDaNotaDeslizada);

  }

  private void removeNota(int posicao) {
    new NotaDAO().remove(posicao);
    adapter.remove(posicao);
  }

  private void trocaNotas(int posicaoInicio, int posicaoFim) {
    new NotaDAO().troca(posicaoInicio, posicaoFim);
    adapter.troca(posicaoInicio, posicaoFim);
  }
}
