package br.com.jorgerabellodev.ceepandroid.ui.activity;

import static br.com.jorgerabellodev.ceepandroid.ui.activity.NotaActivityConstantes.CHAVE_NOTA;
import static br.com.jorgerabellodev.ceepandroid.ui.activity.NotaActivityConstantes.CHAVE_POSICAO;
import static br.com.jorgerabellodev.ceepandroid.ui.activity.NotaActivityConstantes.CODIGO_REQUISICAO_ALTERA_NOTA;
import static br.com.jorgerabellodev.ceepandroid.ui.activity.NotaActivityConstantes.CODIGO_REQUISICAO_INSERE_NOTA;
import static br.com.jorgerabellodev.ceepandroid.ui.activity.NotaActivityConstantes.POSICAO_INVALIDA;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import br.com.jorgerabellodev.ceepandroid.R;
import br.com.jorgerabellodev.ceepandroid.dao.NotaDAO;
import br.com.jorgerabellodev.ceepandroid.model.Nota;
import br.com.jorgerabellodev.ceepandroid.ui.recycler.adapter.ListaNotasAdapter;
import br.com.jorgerabellodev.ceepandroid.ui.recycler.adapter.listener.OnItemClickListener;
import br.com.jorgerabellodev.ceepandroid.ui.recycler.helper.callback.NotaItemTouchHelperCallback;
import java.util.List;

public class ListaNotasActivity extends AppCompatActivity {


  public static final String TITULO_APP_BAR = "Notas";
  private ListaNotasAdapter adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_lista_notas);
    setTitle(TITULO_APP_BAR);
    List<Nota> todasNotas = pegaTodasNotas();
    configuraRecyclerView(todasNotas);
    configuraBotaoInsereNota();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (data != null) {
      preparaDadosParaCadastrar(requestCode, resultCode, data);
      preparaDadosParaAlterar(requestCode, resultCode, data);
    }
  }

  private List<Nota> pegaTodasNotas() {

    NotaDAO dao = new NotaDAO();

    return dao.todos();
  }

  private void preparaDadosParaAlterar(int requestCode, int resultCode, @NonNull Intent data) {
    if (ehResultadoAlteraNota(requestCode, data)) {
      if (resultadoOK(resultCode)) {
        Nota notaRecebida = (Nota) data.getSerializableExtra(CHAVE_NOTA);
        int posicaoRecebida = data.getIntExtra(CHAVE_POSICAO, POSICAO_INVALIDA);

        if (ehPosicaoValida(posicaoRecebida)) {
          altera(notaRecebida, posicaoRecebida);
        }
      }
    }
  }

  private void preparaDadosParaCadastrar(int requestCode, int resultCode, @NonNull Intent data) {
    if (ehResultadoInsereNota(requestCode, data)) {
      if (resultadoOK(resultCode)) {
        Nota notaRecebida = (Nota) data.getSerializableExtra(CHAVE_NOTA);
        adiciona(notaRecebida);
      }
    }
  }

  private void configuraBotaoInsereNota() {
    TextView botaoInsereNota = findViewById(R.id.lista_notas_insere_nota);
    botaoInsereNota.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        abreFormularioNotaActivityParaCadastrar();
      }
    });
  }

  private void abreFormularioNotaActivityParaCadastrar() {
    Intent iniciaFormularioNota = new Intent(ListaNotasActivity.this, FormulatioNotaActivity.class);
    startActivityForResult(iniciaFormularioNota, CODIGO_REQUISICAO_INSERE_NOTA);
  }

  private void abreFormularioNotaActivityParaAlterar(Nota nota, int posicao) {
    Intent abreFormularioComNota = new Intent(ListaNotasActivity.this,
        FormulatioNotaActivity.class);

    abreFormularioComNota.putExtra(CHAVE_NOTA, nota);
    abreFormularioComNota.putExtra(CHAVE_POSICAO, posicao);

    startActivityForResult(abreFormularioComNota, CODIGO_REQUISICAO_ALTERA_NOTA);
  }

  private void configuraRecyclerView(List<Nota> todasNotas) {
    RecyclerView listaNotas = findViewById(R.id.lista_notas_recyclerview);
    configuraAdapter(todasNotas, listaNotas);
    configuraItemTouchHelper(listaNotas);
  }

  private void configuraItemTouchHelper(RecyclerView listaNotas) {
    ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new NotaItemTouchHelperCallback(adapter));
    itemTouchHelper.attachToRecyclerView(listaNotas);
  }

  private void configuraAdapter(List<Nota> todasNotas, RecyclerView listaNotas) {
    adapter = new ListaNotasAdapter(this, todasNotas);
    listaNotas.setAdapter(adapter);
    adapter.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(Nota nota, int posicao) {

        abreFormularioNotaActivityParaAlterar(nota, posicao);
      }
    });
  }

  private void altera(Nota nota, int posicao) {
    new NotaDAO().altera(posicao, nota);
    adapter.altera(posicao, nota);
  }

  private void adiciona(Nota notaRecebida) {
    new NotaDAO().insere(notaRecebida);
    adapter.adiciona(notaRecebida);
  }

  private boolean ehPosicaoValida(int posicaoRecebida) {
    return posicaoRecebida > POSICAO_INVALIDA;
  }

  private boolean ehResultadoAlteraNota(int requestCode, @Nullable Intent data) {
    return ehCodigoRequisicaoAlteraNota(requestCode) && temNota(data);
  }

  private boolean ehCodigoRequisicaoAlteraNota(int requestCode) {
    return requestCode == CODIGO_REQUISICAO_ALTERA_NOTA;
  }

  private boolean ehResultadoInsereNota(int requestCode, @Nullable Intent data) {
    return ehCodigoRequisicaoInsereNota(requestCode) && temNota(data);
  }

  private boolean temNota(@Nullable Intent data) {
    return data != null && data.hasExtra(CHAVE_NOTA);
  }

  private boolean resultadoOK(int resultCode) {
    return resultCode == RESULT_OK;
  }

  private boolean ehCodigoRequisicaoInsereNota(int requestCode) {
    return requestCode == CODIGO_REQUISICAO_INSERE_NOTA;
  }

}
